﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DP_HW2_Facade_Altynbekov
{
    public class Burger
    {
        public string Meat { get; set; }
        public string Bun { get; set; }
        public string Lettuce { get; set; }
        public string Tomato { get; set; }
        public string Cheese { get; set; }

#region Constructors

        public Burger() { }

        public Burger(string meat, string bun, string lettuce, string tomato) 
        {
            Meat = meat;
            Bun = bun;
            Lettuce = lettuce;
            Tomato = tomato;
        }

        public Burger(string meat, string bun, string lettuce, string tomato, string cheese)
        {
            Meat = meat;
            Bun = bun;
            Lettuce = lettuce;
            Tomato = tomato;
            Cheese = cheese;
        }
#endregion

#region Methods
        public void CreateBurger()
        {           
            Console.WriteLine("Жарим " + Meat + ", режем "
                + Bun + " напополам,\nкладем " 
                + Lettuce + ", " + Tomato +
                " и обжаренный\n" + Meat + ".");
            Console.WriteLine("Заказ готов!");
        }

        public void CreateBurgerWithCheese()
        {           
            Console.WriteLine("Жарим " + Meat + ", режем "
                + Bun + " напополам,\nкладем "
                + Lettuce + ", " + Tomato + ", "
                + Cheese + "\nи обжаренный " + Meat + ".");
            Console.WriteLine("Заказ готов!");
        }
#endregion
    }
}
