﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DP_HW2_Facade_Altynbekov
{
    class Program
    {
        
        public static void Main(string[] args)
        {
            Cashier cashier = new Cashier();
            Cook cook = new Cook();

            Facade cc = new Facade(cashier, cook);

            Client client = new Client();
            client.OrderBurger(cc);

            Console.Read();
        }
    }
}
