﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DP_HW2_Facade_Altynbekov
{
    class Client
    {
        public void OrderBurger(Facade facade)
        {
            facade.Start();
            facade.Stop();
        }
    }
}
