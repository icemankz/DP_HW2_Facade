﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DP_HW2_Facade_Altynbekov
{
    class Cook : Cashier
    {      
        public void CookBurger()
        {
            Console.WriteLine("Заказ №" + ordNumb + " принят на исполнение!");
            Burger b1 = new Burger("фарш из мраморной говядины",
                "булочку с кунжутом", "лист свежего салата",
                "кружок помидора черри", "сыр Моцаррелла");
            if (otv == "да")
                b1.CreateBurgerWithCheese();
            else if (otv == "нет")
                b1.CreateBurger();          
        }

        public void OrderFinish()
        {
            Console.WriteLine("Заберите заказ №" + ordNumb + " на стойке!");
        }
    }
}
