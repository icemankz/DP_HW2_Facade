﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DP_HW2_Facade_Altynbekov
{
    class Cashier
    {
        public static string otv { get; set; } // передать ответ повару
        public static int ordNumb { get; set; } // передать номер заказа повару
       
        public void TakeAnOrder()
        {
            Console.WriteLine("Свободная касса!");
            Console.WriteLine("Добрый день! Что будете заказывать?");
            string burg = Console.ReadLine(); // бургер           
        link1:
            Console.WriteLine("С сыром или без: да/нет");

            string o = Console.ReadLine();
            if (o == "да") { burg = "бургер с сыром"; }
            else if (o == "нет") { burg = "бургер без сыра"; }
            else
            {
                Console.WriteLine("Отвечайте только да или нет пожалуйста!");
                goto link1;
            }
            otv = o;
            int count = 0;
            int orderNum = ++count;
            ordNumb = orderNum;
            Console.WriteLine("Заказ №" + ordNumb + " отправлен на исполнение!");
            Console.WriteLine("Ваш заказ " + burg + " за номером " + ordNumb + " будет готов через 3 минуты.");         
        }    
    }
}
