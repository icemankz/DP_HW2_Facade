﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DP_HW2_Facade_Altynbekov
{
    class Facade
    {
        Cashier cashier;
        Cook cook;
        Burger burger;

        public Facade(Cashier cas, Cook co)
        {
            this.cashier = cas;
            this.cook = co;          
        }
        public void Start()
        {
            cashier.TakeAnOrder();
            cook.CookBurger();
        }
        public void Stop()
        {
            cook.OrderFinish();
        }
    }
}
